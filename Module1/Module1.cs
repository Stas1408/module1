﻿using System;

namespace M1
{
    public class Module1
    {
        static void Main(string[] args)
        {
            
        }


        public int[] SwapItems(int a, int b)
        {
            int c = a;
            a = b;
            b = c;
            return new int[2] { a, b };
        }

        public int GetMinimumValue(int[] input)
        {
            int min = input[0];
            for (int i = 0; i < input.Length; i++)
            {
                if (min > input[i])
                {
                    min = input[i];
                }
            }
            return min;
        }
    }
}
